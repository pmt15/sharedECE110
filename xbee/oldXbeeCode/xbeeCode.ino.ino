/* What this code is designed to do: 

 - We will push a button and send a character. Button will be connected to 
- Receive 00, 01, or 10 and light up two LEDs accordingly. [this will come later.]
- We *will* send to a serial port for troubleshooting purposes. 

   CREDITS: 

   Jeffrey Wubbenhorst
   ECE Lab Section 5 
   
 */

 
 
#include <SoftwareSerial.h>
#define Rx 10 // DOUT to pin 10
#define Tx 11 // DIN to pin 11
int button=7;  // read pushbutton from pin 9. (This may vary based on hardware configurations.)
int led1=13; // write led from pin 8. (This may vary based on hardware configurations.)
int led2=12; // we don't need this right now, but it may (will) come in handy later on. 
bool buttonState=false;
char outgoing = 'J'; // ...send a J for Jeffrey if we end up sending something

SoftwareSerial Xbee (Rx, Tx);

void setup() {
  Serial.begin(9600); // Set to No line ending;
  Xbee.begin(9600); // type a char, then hit enter
  delay(500);
  pinMode(button,INPUT); // set the pre-defined pin to an input.
}

void loop() {
   if(Serial.available()) { // If serial data available...

     char outgoing = Serial.read(); // uncomment this line to send a character from serial data
  /*  
   *   
   buttonState=digitalRead(button); // read the state of the button. 
    if(buttonState==HIGH){ // ...if the button is pushed...
         Xbee.print(outgoing); // ...print a character to xbee (in this case, 'J')
    
    }
    */
    Xbee.print(outgoing); // ...print a character to xbee (in this case, 'J')
   }
 if(Xbee.available()) { // Is data available from XBee?
    char incoming = Xbee.read(); // Read character from xbee; 
    Serial.println(incoming); // send what's on the air to the serial monitor
    if(incoming!=' '){
    
    digitalWrite(led1,HIGH); // write the LED as high 
    
    delay(500); // wait half a second 
    digitalWrite(led1,LOW); // turn the LED off again 
    }
    }
  
      // do something
  delay(50); // run every 50 milliseconds. 
   }
  

